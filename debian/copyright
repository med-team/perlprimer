Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/owenjm/perlprimer/archive/02c59f31dd3637fefef865edb6dff779d077dce7.tar.gz
Upstream-Name: PerlPrimer
Upstream-Contact: http://perlprimer.sourceforge.net/contact.html

Files: *
Copyright: © 2003-2017 Owen Marshall <owenjm@users.sourceforge.net>
License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
Comment: On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in ‘/usr/share/common-licenses/GPL-2’.

Files: gcg.603
Copyright: © 2006 Dr. Richard J. Roberts
License: other
 This file is from REBASE version 603.
 .
 REBASE, the Restriction Enzyme Database
 Dr. R.J. Roberts and D. Macelis
 rebase.neb.com
 .
 "REBASE is free to its users and they may use the data however they'd like,
 but when this use involves repackaging the data and redistribututing it, the
 common courtesy we always provide one another is to credit the source of this
 data." (Private communication to C. Plessy)

Files: debian/*
Copyright: © 2006–2011 Charles Plessy <plessy@debian.org>
License: BOLA
 The work of CP is licenses as follows.  License of the rest is unknown.
 .
  BOLA - Buena Onda License Agreement (v1.1)
  ------------------------------------------
 .
 This work is provided 'as-is', without any express or implied warranty. In no
 event will the authors be held liable for any damages arising from the use of
 this work.
 .
 To all effects and purposes, this work is to be considered Public Domain.
 .
 .
 However, if you want to be "buena onda", you should:
 .
  1. Not take credit for it, and give proper recognition to the authors.
  2. Share your modifications, so everybody benefits from them.
  3. Do something nice for the authors.
  4. Help someone who needs it: sign up for some volunteer work or help your
     neighbour paint the house.
  5. Don't waste. Anything, but specially energy that comes from natural
     non-renewable resources. Extra points if you discover or invent something
     to replace them.
  6. Be tolerant. Everything that's good in nature comes from cooperation.
