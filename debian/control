Source: perlprimer
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
           Charles Plessy <plessy@debian.org>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/med-team/perlprimer
Vcs-Git: https://salsa.debian.org/med-team/perlprimer.git
Homepage: http://perlprimer.sourceforge.net
Rules-Requires-Root: no

Package: perlprimer
Architecture: all
Depends: ${misc:Depends},
         perl-tk,
         libwww-perl
Recommends: ncbi-tools-bin
Suggests: perlprimer-doc
Description: Graphical design of primers for PCR
 PerlPrimer is a free, open-source GUI application written in Perl that designs
 primers for standard Polymerase Chain Reaction (PCR), bisulphite PCR,
 real-time PCR (QPCR) and sequencing. It aims to automate and simplify the
 process of primer design.
 .
 If operated online, the tool nicely communicates with the Ensembl
 project for further insights into the gene structure, i.e., allowing
 for taking the location of exons and introns into account for the design
 of the primers.  The sequences themselves can be retrieved, too.

Package: perlprimer-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Suggests: perlprimer
Description: Tutorial to perlprimer
 PerlPrimer is a free, open-source GUI application written in Perl that designs
 primers for standard Polymerase Chain Reaction (PCR), bisulphite PCR,
 real-time PCR (QPCR) and sequencing. It aims to automate and simplify the
 process of primer design.
 .
 If operated online, the tool nicely communicates with the Ensembl
 project for further insights into the gene structure, i.e., allowing
 for taking the location of exons and introns into account for the design
 of the primers.  The sequences themselves can be retrieved, too.
 .
 An HTML tutorial explaining the use of the user interface and
 utilisation of online resources.
